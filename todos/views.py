from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list_list": todo,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    task = TodoList.objects.get(id=id)
    context = {
        "todo_list_detail": task,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    task = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoListForm(instance=task)

        context = {
            "form": form,
        }
        return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    task = TodoList.objects.get(id=id)
    if request.method == "POST":
        task.delete()
        return redirect("todo_list_list")
    context = {"task": task}
    return render(request, "todos/delete.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item)

    context = {"form": form}
    return render(request, "todos/edit_item.html", context)
